package me.muhajirin.scenes;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import me.muhajirin.scenes.atm.AtmLoginScene;
import me.muhajirin.scenes.teller.TellerLoginScene;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainScene implements Initializable {

    public Button mTellerBtn;

    public Button mAtmBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        mTellerBtn.setOnMouseClicked(e -> {
            System.out.println(" : user choose teller");
            openLoginPage("Teller", TellerLoginScene.class.getResource("login.fxml"));

            ((Node)(e.getSource())).getScene().getWindow().hide();
        });

        mAtmBtn.setOnMouseClicked(e -> {
            System.out.println(" : user choose atm");
            openLoginPage("ATM", AtmLoginScene.class.getResource("login.fxml"));

            ((Node)(e.getSource())).getScene().getWindow().hide();
        });

    }

    public static void openLoginPage(String title, URL location) {
        try {
            Parent root = FXMLLoader.load(location);

            Stage stage = new Stage();
            stage.setTitle(title);
            stage.setScene(new Scene(root, 600, 300));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
