package me.muhajirin.scenes.atm;

import com.dlsc.formsfx.model.structure.*;
import com.dlsc.formsfx.model.util.BindingMode;
import com.dlsc.formsfx.view.renderer.FormRenderer;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import me.muhajirin.bank.Container;
import me.muhajirin.scenes.transactions.MainTransactionScene;
import org.controlsfx.control.Notifications;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class AtmLoginScene implements Initializable {

    public StackPane pane;

    private class FormModel {
        StringProperty serialNumber, pin;

        FormModel() {
            this.serialNumber = new SimpleStringProperty();
            this.pin = new SimpleStringProperty();
        }
    }

    private FormModel mFormModel = new FormModel();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        StringField cardSerialNumber = Field.ofStringType("").label("Serial Number")
                .bind(mFormModel.serialNumber).required("This field cannot be empty");
        PasswordField cardPin = Field.ofPasswordType("").label("Pin")
                .bind(mFormModel.pin).required("This field cannot be empty");

        Form loginForm = Form.of(Group.of(cardSerialNumber, cardPin)).title("Login");
        loginForm.binding(BindingMode.CONTINUOUS);

        Button submitButton = new Button("Masuk");

        submitButton.setOnMouseClicked(e -> {
            System.out.println(" : -------------------------------------------------");
            System.out.println(" : serial number : "  + cardSerialNumber.getValue());
            System.out.println(" : pin           : "  + mFormModel.pin.get());

            try {
                Container.getInstance()
                        .loginWithCard(mFormModel.serialNumber.get(), mFormModel.pin.get());

                openTransactionPage(e);
            } catch (RuntimeException err) {
                Notifications.create().text(err.getMessage()).showError();
            }
        });

        pane.getChildren().add(new FormRenderer(loginForm));
        pane.getChildren().add(submitButton);
    }

    public static void openTransactionPage(MouseEvent e) {
        System.out.println(" : -------------------------------------------------");
        System.out.println(" : user " + Container.getInstance().getActiveSessionAccount().getName() + " logged in !");

        try {
            Stage stage = new Stage();
            stage.setTitle("Transaction");
            stage.setScene(new Scene(FXMLLoader.load(MainTransactionScene.class.getResource("main_transaction.fxml")), 600, 300));
            stage.show();
        } catch (IOException err) {
            err.printStackTrace();
        }

        ((Node)(e.getSource())).getScene().getWindow().hide();
    }

}
