package me.muhajirin.scenes.teller;

import com.dlsc.formsfx.model.structure.*;
import com.dlsc.formsfx.model.util.BindingMode;
import com.dlsc.formsfx.view.renderer.FormRenderer;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import me.muhajirin.bank.Container;
import me.muhajirin.bank.exceptions.AccountNotFoundException;
import me.muhajirin.scenes.atm.AtmLoginScene;
import me.muhajirin.scenes.transactions.MainTransactionScene;
import org.controlsfx.control.Notifications;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TellerLoginScene implements Initializable {

    public StackPane pane;

    private class FormModel {
        StringProperty id, signature;

        FormModel() {
            this.id = new SimpleStringProperty();
            this.signature = new SimpleStringProperty();
        }
    }

    private FormModel mFormModel = new FormModel();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        StringField AccountIdentity = Field.ofStringType("").label("Nomer Rekening")
                .bind(mFormModel.id).required("This field cannot be empty");
        PasswordField AccountSignature = Field.ofPasswordType("").label("Tanda Tangan")
                .bind(mFormModel.signature).required("This field cannot be empty");

        Form loginForm = Form.of(Group.of(AccountIdentity, AccountSignature)).title("Login");
        loginForm.binding(BindingMode.CONTINUOUS);

        Button submitButton = new Button("Masuk");

        submitButton.setOnMouseClicked(e -> {
            System.out.println(" : -------------------------------------------------");
            System.out.println(" : Identity  : "  + Long.parseLong(mFormModel.id.get()));
            System.out.println(" : Signature : "  + Long.parseLong(mFormModel.signature.get()));

            try {
                Container.getInstance()
                        .loginWithAccount((mFormModel.id.get()), (mFormModel.signature.get()));

                AtmLoginScene.openTransactionPage(e);
            } catch (RuntimeException err) {
                Notifications.create().text(err.getMessage()).showError();
            }
        });

        pane.getChildren().add(new FormRenderer(loginForm));
        pane.getChildren().add(submitButton);
    }

}
