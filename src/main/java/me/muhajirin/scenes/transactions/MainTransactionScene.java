package me.muhajirin.scenes.transactions;

import com.dlsc.formsfx.model.structure.*;
import com.dlsc.formsfx.model.util.BindingMode;
import com.dlsc.formsfx.view.renderer.FormRenderer;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.StackPane;
import me.muhajirin.MainApp;
import me.muhajirin.bank.Account;
import me.muhajirin.bank.Container;
import me.muhajirin.payment.Transaction;
import me.muhajirin.scenes.MainScene;
import org.controlsfx.control.Notifications;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class MainTransactionScene implements Initializable {

    public StackPane mDepositPane;
    public StackPane mWithdrawPane;
    public StackPane mTransferPane;
    public StackPane mPaymentPane;
    public Label mRequestBalanceLabel;
    public Tab mRequestBalanceTab;
    public Button mBtnLogout;

    private Container container = Container.getInstance();

    private Account account = container.getActiveSessionAccount();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bootDepositPane();
        bootWithdrawPane();
        bootRequestBalancePane();
        bootTransferPane();
        bootPaymentsPane();

        mBtnLogout.setOnMouseClicked(e -> {
            System.out.println(" : Logging out");

            container.logout();
            MainApp.mainStage.show();

            ((Node)(e.getSource())).getScene().getWindow().hide();
        });
    }

    private void bootDepositPane() {
        IntegerField valueDeposit = Field.ofIntegerType(0).label("Nominal");
        Button submitButton = new Button("Deposit");

        mDepositPane.getChildren().add(new FormRenderer(Form.of(Group.of(valueDeposit))));
        mDepositPane.getChildren().add(submitButton);

        submitButton.setOnMouseClicked(e -> {
            System.out.println(" : -------------------------------------------------");
            System.out.println(" : Deposit : " + valueDeposit.getValue());

            try {
                container.deposit((long) valueDeposit.getValue());
                Notifications.create().text("Deposit Success!. Your balance : Rp " + account.getBalance())
                        .showInformation();
                valueDeposit.reset();
            } catch (RuntimeException err) {
                Notifications.create().text(err.getMessage()).showError();
            }
        });
    }

    private void bootWithdrawPane() {
        IntegerField valueWithdraw = Field.ofIntegerType(0).label("Nominal");
        Button submitButton = new Button("Withdraw");

        mWithdrawPane.getChildren().add(new FormRenderer(Form.of(Group.of(valueWithdraw))));
        mWithdrawPane.getChildren().add(submitButton);

        submitButton.setOnMouseClicked(e -> {
            System.out.println(" : -------------------------------------------------");
            System.out.println(" : Withdraw : " + valueWithdraw.getValue());

            try {
                if (container.withdrawal((long) valueWithdraw.getValue()))
                    Notifications.create().text("Withdraw Success!. Your balance : Rp " + account.getBalance())
                            .showInformation();
                valueWithdraw.reset();
            } catch (RuntimeException err) {
                Notifications.create().text(err.getMessage()).showError();
            }

        });
    }

    private void bootRequestBalancePane() {
        mRequestBalanceTab.setOnSelectionChanged(e -> mRequestBalanceLabel.setText("Rp " + account.getBalance() + ",-"));
    }

    private void bootTransferPane() {
        StringField accountIdentity = Field.ofStringType("").label("Akun tujuan");
        IntegerField amount = Field.ofIntegerType(0).label("Nominal");
        Button submitButton = new Button("Transfer");

        mTransferPane.getChildren().add(new FormRenderer(Form.of(Group.of(accountIdentity, amount))));
        mTransferPane.getChildren().add(submitButton);

        submitButton.setOnMouseClicked(e -> {
            System.out.println(" : -------------------------------------------------");
            System.out.println(" : Transfer : " + amount.getValue());

            try {
                Account destination = container.transfer(accountIdentity.getValue(), (long) amount.getValue());

                if (destination != null)
                    Notifications.create().text("Transfer Success!. Your balance : Rp " + account.getBalance())
                            .showInformation();

                accountIdentity.reset();
                amount.reset();
            } catch (RuntimeException err) {
                Notifications.create().text(err.getMessage()).showError();
            }

        });
    }

    private void bootPaymentsPane() {
        Transaction transaction = Transaction.getInstance();

        SingleSelectionField<String> selectionField = Field.ofSingleSelectionType(new ArrayList<String>() {
            {
                add("PLN");
                add("PDAM");
            }
        }).label("Products");

        StringField subscriberID = Field.ofStringType("").label("Nomor Pelanggan")
                .bind(transaction.subscriberID);
        IntegerField valueInvoice = Field.ofIntegerType(0).label("Nominal").editable(false)
                .bind(transaction.inquiryAmount);

        valueInvoice.persist();

        Button submitButton = new Button("Bayar");

        Form form = Form.of(Group.of(selectionField, subscriberID, valueInvoice));
        form.binding(BindingMode.CONTINUOUS);

        mPaymentPane.getChildren().add(new FormRenderer(form));
        mPaymentPane.getChildren().add(submitButton);

        submitButton.setOnMouseClicked(e -> {
            System.out.println(" : -------------------------------------------------");
            System.out.println(" : Payment");

            System.out.println(" : Amount : " + valueInvoice.getValue());

            try {
                if (container.pay((long) valueInvoice.getValue()))
                    Notifications.create().text(
                            "Payment " + selectionField.getSelection() + " success!. Your balance : Rp " + account.getBalance()
                    ).showInformation();
                subscriberID.reset();
                valueInvoice.reset();
            } catch (RuntimeException err) {
                Notifications.create().text(err.getMessage()).showError();
            }
        });
    }

}
