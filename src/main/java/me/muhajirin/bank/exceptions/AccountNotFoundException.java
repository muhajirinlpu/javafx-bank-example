package me.muhajirin.bank.exceptions;

public class AccountNotFoundException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Account Not Found";
    }
}
