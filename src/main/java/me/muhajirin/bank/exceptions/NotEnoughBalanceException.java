package me.muhajirin.bank.exceptions;

public class NotEnoughBalanceException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Not Enough Balance";
    }
}
