package me.muhajirin.bank.cards;

import me.muhajirin.bank.Account;

public class GoldCard extends Card {
    public GoldCard(Account account, String pin) {
        super(account, pin);
    }

    @Override
    public String getType() {
        return TYPE_GOLD;
    }

    @Override
    public double getTransactionLimit() {
        return 50000000;
    }
}
