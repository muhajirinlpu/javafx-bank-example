package me.muhajirin.bank.cards;

import me.muhajirin.bank.Account;
import me.muhajirin.bank.cards.contracts.CardAble;

import java.util.Random;
import java.util.stream.IntStream;

abstract public class Card implements CardAble {

    public static final String TYPE_SILVER = "SILVER";

    public static final String TYPE_GOLD = "GOLD";

    private String serial_number;

    private Account account;

    private final String pin;

    Card(Account account, String pin) {
        this.serial_number = String.valueOf(((new Random()).nextLong() % 100000000000000L) + 5200000000000000L);

        this.account = account;

        this.pin = pin;
    }

    @Override
    public String getSerialNumber() {
        return this.serial_number;
    }

    @Override
    public Account getAccount() {
        return account;
    }

    @Override
    public Boolean pinVerify(String pin) {
        return pin.equals(this.pin);
    }
}
