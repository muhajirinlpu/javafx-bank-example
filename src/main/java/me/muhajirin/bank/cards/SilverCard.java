package me.muhajirin.bank.cards;

import me.muhajirin.bank.Account;

public class SilverCard extends Card {
    public SilverCard(Account account, String pin) {
        super(account, pin);
    }

    @Override
    public String getType() {
        return TYPE_SILVER;
    }

    @Override
    public double getTransactionLimit() {
        return 10000000;
    }
}
