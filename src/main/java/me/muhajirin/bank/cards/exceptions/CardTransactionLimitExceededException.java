package me.muhajirin.bank.cards.exceptions;

public class CardTransactionLimitExceededException extends RuntimeException {
    @Override
    public String getMessage() {
        return "Card transaction limit";
    }
}
