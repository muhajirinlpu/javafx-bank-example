package me.muhajirin.bank.cards.contracts;

import me.muhajirin.bank.cards.exceptions.CardNotFoundException;

public interface HasCards {

    boolean addCard(String type, String pin);

    CardAble findCard(String serialNumber) throws CardNotFoundException;

}
