package me.muhajirin.bank.cards.contracts;

import me.muhajirin.bank.contracts.Accountable;

public interface CardAble {

    public String getSerialNumber();

    public String getType();

    public Accountable getAccount();

    public Boolean pinVerify(String pin);

    public double getTransactionLimit();
}
