package me.muhajirin.bank.cards.contracts;

import me.muhajirin.bank.cards.Card;

public interface Transactionable {

    public Card getActiveCard();

    public boolean setActiveCard(String serialNumber, String pin);

}
