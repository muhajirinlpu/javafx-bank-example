package me.muhajirin.bank.contracts;

import me.muhajirin.bank.exceptions.NotEnoughBalanceException;

import java.util.Date;

public interface Accountable {

    public String getId();

    public String getSignature();

    public String getNik();

    public String getName();

    public String getAddress();

    public String getBirthPlace();

    public Date getBirthDate();

    public String getPhoneNumber();

    public long getBalance();

    public void deposit(long value);

    public boolean withdrawal(long value) throws NotEnoughBalanceException;
}
