package me.muhajirin.bank;

import me.muhajirin.bank.cards.Card;
import me.muhajirin.bank.cards.exceptions.CardNotFoundException;
import me.muhajirin.bank.cards.exceptions.CardTransactionLimitExceededException;
import me.muhajirin.bank.exceptions.AccountNotFoundException;
import me.muhajirin.bank.exceptions.NotEnoughBalanceException;

import java.util.ArrayList;
import java.util.Calendar;

public final class Container {

    public class Bank {
        private ArrayList<Account> accounts;

        Bank() {
            this.accounts = new ArrayList<>();
        }

        Account getAccount(String id) {
            for (Account account : this.accounts) {
                if (account.getId().equals(id))
                    return account;
            }

            throw new AccountNotFoundException();
        }

        boolean registerNewAccount(Account account) {
            return this.accounts.add(account);
        }
    }

    private static Container instance = new Container();

    public static Container getInstance() {
        return instance;
    }

    public Bank bank;

    public Account getActiveSessionAccount() {
        return activeSessionAccount;
    }

    private Account activeSessionAccount;

    private String activeSessionType;

    private static final String TYPE_TELLER = "teller";

    private static final String TYPE_ATM = "atm";

    private Container() {
        this.bank = new Bank();
    }

    public void seedFakeData() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1999, Calendar.MAY, 23);

        // me
        Account me = new Account(
                "3507012305990001", "Muhajirin Ida Ilyas",
                "Jl Jojoran III/ 48", "Malang", calendar.getTime(),
                "085107680400"
        );

        if (me.addCard(Card.TYPE_GOLD, "666666")) {
            System.out.println("New card added    : " + me.getCards().get(0).getSerialNumber() );
        }

        if (this.bank.registerNewAccount(me)) {
            System.out.println("New account added : " + me.getId() + " : " + me.getSignature());
        }

        // other
        calendar.set(1991, Calendar.NOVEMBER, 12);
        Account other = new Account(
                "35070121117728839", "Jon Snow",
                "Winterfel", "Dorne", calendar.getTime(),
                "00123"
        );

        if (other.addCard(Card.TYPE_GOLD, "666666")) {
            System.out.println("New card added    : " + other.getCards().get(0).getSerialNumber() );
        }

        if (this.bank.registerNewAccount(other)) {
            System.out.println("New account added : " + other.getId() + " : " + other.getSignature());
        }
    }

    public void loginWithAccount(String id, String signature) throws AccountNotFoundException {
        for (Account account : this.bank.accounts) {
            if (id.equals(account.getId()) && signature.equals(account.getSignature())) {
                this.activeSessionType = TYPE_TELLER;
                this.activeSessionAccount = account;
                return;
            }
        }

        throw new AccountNotFoundException();
    }

    public void loginWithCard(String serialNumber, String pin) throws CardNotFoundException {
        for (Account account : this.bank.accounts) {
            try {
                if (account.setActiveCard(serialNumber, pin)) {
                    this.activeSessionType = TYPE_ATM;
                    this.activeSessionAccount = account;

                    return;
                }
            } catch (CardNotFoundException ignored) {}
        }

        throw new CardNotFoundException();
    }

    private void protectCardTransaction(Long value) {
        if (activeSessionType.equals(TYPE_ATM)) {
            if (value > activeSessionAccount.getActiveCard().getTransactionLimit())
                throw new CardTransactionLimitExceededException();
        }
    }

    public void logout() {
        this.activeSessionAccount = null;
    }

    public void deposit(Long value) throws CardTransactionLimitExceededException {
        protectCardTransaction(value);

        activeSessionAccount.deposit(value);
    }

    public boolean withdrawal(Long value) throws CardTransactionLimitExceededException, NotEnoughBalanceException {
        protectCardTransaction(value);

        return activeSessionAccount.withdrawal(value);
    }

    public Account transfer(String accountId, Long amount) {
        protectCardTransaction(amount);

        Account destination = this.bank.getAccount(accountId);

        this.activeSessionAccount.withdrawal(amount);
        destination.deposit(amount);

        return destination;
    }

    public boolean pay(Long inquiryAmount) {
        protectCardTransaction(inquiryAmount);

        return this.activeSessionAccount.withdrawal(inquiryAmount);
    }
}
