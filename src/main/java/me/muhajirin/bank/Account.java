package me.muhajirin.bank;

import me.muhajirin.bank.cards.Card;
import me.muhajirin.bank.cards.GoldCard;
import me.muhajirin.bank.cards.SilverCard;
import me.muhajirin.bank.cards.contracts.Transactionable;
import me.muhajirin.bank.cards.exceptions.CardNotFoundException;
import me.muhajirin.bank.contracts.Accountable;
import me.muhajirin.bank.cards.contracts.CardAble;
import me.muhajirin.bank.cards.contracts.HasCards;
import me.muhajirin.bank.exceptions.NotEnoughBalanceException;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

public class Account implements Accountable, HasCards, Transactionable {

    private final ArrayList<Card> cards;
    private Card activeCard;

    private Date birthDate;
    private String birthPlace;
    private String phoneNumber;
    private String address;
    private String name;
    private String nik;
    private String id;
    private String signature;
    private long balance;

    Account(String nik, String name, String address, String birthPlace, Date birthDate, String phoneNumber) {
        this.id = Long.toString(((new Random()).nextLong() % 1000000L) + 5200000L);
        this.signature = Long.toString(((new Random()).nextLong() % 1000000L) + 5200000L);
        this.nik = nik;
        this.name = name;
        this.address = address;
        this.birthPlace = birthPlace;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.balance = 0;

        this.cards = new ArrayList<>();
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getSignature() {
        return signature;
    }

    @Override
    public String getNik() {
        return nik;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public String getBirthPlace() {
        return birthPlace;
    }

    @Override
    public Date getBirthDate() {
        return birthDate;
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public void deposit(long value) {
        this.balance += value;
    }

    @Override
    public boolean withdrawal(long value) throws NotEnoughBalanceException {
        if (value > balance) throw new NotEnoughBalanceException();

        this.balance -= value;

        return true;
    }

    @Override
    public long getBalance() {
        return balance;
    }

    @Override
    public boolean addCard(String type, String pin) {
        switch (type) {
            case Card.TYPE_GOLD:
                return this.cards.add(new GoldCard(this, pin));
            case Card.TYPE_SILVER:
                return this.cards.add(new SilverCard(this, pin));
            default:
                return false;
        }
    }

    @Override
    public CardAble findCard(String serialNumber) throws CardNotFoundException {
        for (Card card : this.cards) {
            if (serialNumber.equals(card.getSerialNumber()))
                return card;
        }

        throw new CardNotFoundException();
    }

    ArrayList<Card> getCards() {
        return cards;
    }

    @Override
    public Card getActiveCard() {
        return activeCard;
    }

    @Override
    public boolean setActiveCard(String serialNumber, String pin) {
        Card card = (Card) this.findCard(serialNumber);

        if (card.pinVerify(pin)) {
            this.activeCard = card;

            return true;
        }

        return false;
    }
}
