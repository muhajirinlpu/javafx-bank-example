package me.muhajirin.payment;

import javafx.beans.property.*;

import java.util.Random;

public class Transaction {
    private static Transaction ourInstance = new Transaction();

    public static Transaction getInstance() {
        return ourInstance;
    }

    public StringProperty subscriberID;

    public IntegerProperty inquiryAmount;

    private Transaction() {
        this.subscriberID = new SimpleStringProperty();
        this.inquiryAmount = new SimpleIntegerProperty();

        this.subscriberID.addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 8) {
                this.inquiryAmount.setValue(((new Random()).nextLong() % 10000L) + 520000L);
            }
        });
    }
}
