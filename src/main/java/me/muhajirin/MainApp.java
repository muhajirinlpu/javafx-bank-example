package me.muhajirin;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import me.muhajirin.bank.Container;
import me.muhajirin.scenes.MainScene;

public class MainApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public static Stage mainStage;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Container.getInstance().seedFakeData();
        Parent root = FXMLLoader.load(MainScene.class.getResource("main.fxml"));
        Scene scene = new Scene(root);

        mainStage = primaryStage;

        primaryStage.setTitle("LD BANK");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
